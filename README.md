# Topology

![infrastucture](Source/img/232.png)


**Terraform => create 3 server**

**Server 1: Gitlab**

**Server 2: Gitlab-runner + Registry**

**Server 3: Deploy**


//Docker install
```javascript
apt-get update
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```


# Server 1: GitLab server

//GitLab install
```javascript
mkdir -p /home/docker-app/gitlab && cd /home/docker-app/gitlab
Create a docker-compose.yml file
docker compose up -d
docker ps
```

//Change root's passwd docker
```javascript
docker exec -it gitlab-server bash
gitlab-rake "gitlab:password:reset[root]"
```
//Notes: Turn off auto DevOps GitLab

# Server 2: GitLab-runner connect to  gitlab server and registry

//gitlab runner setup
``` javascript
apt-get update
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt-get install gitlab-runner
apt-cache madison gitlab-runner
gitlab-runner -version
usermod -aG docker gitlab-runner
usermod -aG sudo gitlab-runner
visudo
gitlab-runner register
```

![sudosu](Source/img/233.png)

//setup registry by docker
``` javascript
mkdir -p /home/docker-app/registry && chmod -R 777 /home/docker-app/registry && cd /home/docker-app/registry
mkdir certs data
apt-get update
apt-get install openssl
```
//Notes: this is private certificate
``` javascript
openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -subj "/CN=13.229.213.87" -addext "subjectAltName = DNS:13.229.213.87,IP:13.229.213.87" -x509 -days 365 -out certs/testgitlab.crt
``` 
Create another docker-compose.yml file

docker-compose up -d

//Auth private key for docker
``` javascript
mkdir -p /etc/docker/certs.d/13.229.213.87:5000/ 
cp /home/docker-app/registry/certs/domain.crt /etc/docker/certs.d/10.32.4.125:5000/ca.crt 
systemctl restart docker restart lại docker
docker login https://13.229.213.87:5000
``` 
# Server 3: CICD Pipeline

Create dockerfile and .gitlab-ci.yml file

//Setup connection between server deploy and registry

**On sv2:** 

``` javascript
passwd gitlab-runner
exit
ssh gitlab-runner@13.229.213.87
ssh-keygen -t rsa
ssh-copy-id root@54.255.230.3
```
**Auth the private cert:**
``` javascript
mkdir -p /home/sshkey/13.229.213.87/
scp root@13.229.213.87:/home/docker-app/registry/certs/domain.crt /home/sshkey/13.229.213.87/
mkdir -p /etc/docker/certs.d/13.229.213.87:5000/
cp /home/sshkey/13.229.213.87/domain.crt /etc/docker/certs.d/13.229.213.87:5000/ca.crt
systemctl restart docker
```
# DONE
