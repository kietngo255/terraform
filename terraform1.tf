terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "ap-southeast-1"
}

resource "aws_instance" "serverone" {
  ami           = "ami-0df7a207adb9748c7"
  instance_type = "t2.micro"
  key_name = "tf_key"

  tags = {
    Name = "serverone"
  }
}

resource "aws_instance" "servertwo" {
    ami             = "ami-0df7a207adb9748c7"
    instance_type   = "t2.micro"
    key_name = "tf_key"

    tags = {
      Name = "servertwo"
    }
}

resource "aws_instance"  "serverthree"{
    ami             = "ami-0df7a207adb9748c7"
    instance_type = "t2.micro"
    key_name = "tf_key"

    tags = {
      Name = "serverthree"
    }
}

data "aws_vpc" "default_vpc"{
  default = true
}

data "aws_subnet_ids" "default_vpc"{
  vpc_id = data.aws_vpc.default_vpc.id
}
